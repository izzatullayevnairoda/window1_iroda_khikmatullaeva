WITH RankedCustomers AS (
    SELECT 
        cust_id,
        RANK() OVER (PARTITION BY channel_id ORDER BY SUM(amount_sold) DESC) AS channel_rank,
        channel_id
    FROM 
        sh.sales
    WHERE 
        EXTRACT(YEAR FROM time_id) IN (1998, 1999, 2001)
    GROUP BY 
        cust_id, channel_id
)
SELECT 
    rc.cust_id AS "Customer ID",
    c.cust_first_name AS "First Name",
    c.cust_last_name AS "Last Name",
    ch.channel_desc AS "Channel",
    RANK() OVER (PARTITION BY rc.channel_id ORDER BY rc.channel_rank) AS "Overall Rank",
    ROUND(SUM(s.amount_sold), 2) AS "Total Sales"
FROM 
    RankedCustomers rc
JOIN 
    sh.customers c ON rc.cust_id = c.cust_id
JOIN 
    sh.sales s ON rc.cust_id = s.cust_id AND rc.channel_id = s.channel_id
JOIN 
    sh.channels ch ON rc.channel_id = ch.channel_id
WHERE 
    rc.channel_rank <= 300
GROUP BY 
    rc.cust_id, c.cust_first_name, c.cust_last_name, ch.channel_desc, rc.channel_id
ORDER BY 
    rc.channel_id, "Overall Rank";
